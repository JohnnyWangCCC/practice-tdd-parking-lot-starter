### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we started with the customary codereview and then began today's exercise. Today is still around the TDD development training, we detailed from the case, to the test code implementation, and then to the business implementation of the project-based exercises, a simple demo around the parking lot, we can clearly perceive a simple demand, after the business side of the requirements of the iterative, and finally produced the business logic redundancy, the update of the unit test, to be able to understand the process from the Unit testing and the importance of scalability. This week we went from concept map, to context map, tasking, unit test, TDD, and formed a set of good code programming habits, which is what we desperately need. The thing that struck me the most is that the evolution of our system from one small requirement to multiple requirements is going to produce bad code. A good code flow will enable us to refactor the business more clearly and provide scalability.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Cheerful.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	It really improved my business writing ability and allowed me to write code with better maintenance, which is very critical for business development.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Today's lesson can be practically applied to future business work, which is the actual process of the project of the team I am interning with. I will incorporate TDD ideas into my future project requirements and follow the correct process for new requirements.