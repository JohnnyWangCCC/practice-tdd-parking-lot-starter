package com.parkinglot;

public class Ticket {
    private String ticketId;

    public Ticket(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Ticket){
            return ticketId.equals(((Ticket)obj).getTicketId());
        }
        return super.equals(obj);
    }

    public String getTicketId() {
        return ticketId;
    }
}
