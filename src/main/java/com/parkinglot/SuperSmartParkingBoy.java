package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy extends Boy{
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLot = parkingLots.stream()
                .filter(ParkingLot::isAvailable)
                .max(Comparator.comparingDouble(ParkingLot::getAvailabilityRatio))
                .orElseThrow(NoAvailablePositionException::new);
        return parkingLot.park(car);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return parkingLots.stream().
                filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnRecognizedTicketException::new);
    }
}
