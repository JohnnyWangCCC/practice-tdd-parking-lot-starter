package com.parkinglot;

import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ParkingLotBoy extends Boy{
    public ParkingLotBoy(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    @Override
    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::isAvailable)
                .findFirst().map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(lot -> lot.fetch(ticket))
                .orElseThrow(UnRecognizedTicketException::new);
    }
}
