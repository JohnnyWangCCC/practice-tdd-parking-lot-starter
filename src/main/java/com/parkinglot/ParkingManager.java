package com.parkinglot;

import java.util.*;

public class ParkingManager extends Boy{
    private Map<String,Boy> boyMap = new HashMap<String,Boy>();

    public ParkingManager(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    public ParkingManager() {
    }

    public void addParkingLotBoy(Boy boy) {
        if (boy instanceof ParkingLotBoy){
            boyMap.put("parkingLotBoy",boy);
        }else if (boy instanceof SmartParkingBoy){
            boyMap.put("smartParkingBoy",boy);
        }else if (boy instanceof SuperSmartParkingBoy){
            boyMap.put("superSmartParkingBoy",boy);
        }
    }

    public Boy getBoy(String parkingLotBoy) {
        return boyMap.get(parkingLotBoy);
    }

    @Override
    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::isAvailable)
                .findFirst().map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    @Override
    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(lot -> lot.fetch(ticket))
                .orElseThrow(UnRecognizedTicketException::new);
    }

    public Ticket notifyBoyPark(String parkingLotBoy, Car car) {
        return this.boyMap.get(parkingLotBoy).park(car);
    }

    public Car notifyBoyFetch(String parkingLotBoy, Ticket ticket) {
        return boyMap.get(parkingLotBoy).fetch(ticket);
    }
}
