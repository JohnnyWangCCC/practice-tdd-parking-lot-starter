package com.parkinglot;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ParkingLot {
    private final Map<String, Car> parkingSpaces = new HashMap<>();

    private int capacity = 3;

    public Ticket park(Car car) {
        if (this.capacity <= parkingSpaces.size()) {
            throw new NoAvailablePositionException();
        }
        Ticket ticket = new Ticket(UUID.randomUUID().toString());
        this.parkingSpaces.put(ticket.getTicketId(), car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!parkingSpaces.containsKey(ticket.getTicketId())) {
            throw new UnRecognizedTicketException();
        }
        Car car = parkingSpaces.get(ticket.getTicketId());
        parkingSpaces.remove(ticket.getTicketId());
        return car;
    }

    public boolean isIn(Ticket ticket){
        return parkingSpaces.containsKey(ticket.getTicketId());
    }

    public int getCapacity() {
        return capacity;
    }

    public int getAvailableCapacity() {
        return capacity - parkingSpaces.size();
    }

    public double getAvailabilityRatio() {
        return new BigDecimal(capacity - parkingSpaces.size()).divide(new BigDecimal(capacity)).doubleValue();
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isAvailable(){
        return this.capacity > parkingSpaces.size();
    }

}
