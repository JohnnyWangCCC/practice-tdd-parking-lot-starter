package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public abstract class Boy {
    protected List<ParkingLot> parkingLots = new ArrayList<>();

    abstract public Ticket park(Car car);

    abstract public Car fetch(Ticket ticket);
}
