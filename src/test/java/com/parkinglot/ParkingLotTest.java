package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class ParkingLotTest {
    @Test
    public void should_return_ticket_when_park_given_a_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);

    }

    @Test
    public void should_return_a_car_when_fetch_given_a_car_and_a_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        Ticket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        Assertions.assertEquals(car, fetchedCar);

    }

    @Test
    public void should_return_two_cars_when_fetch_given_two_cars_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();

        //when
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);

    }

    @Test
    public void should_return_nothing_when_fetch_given_a_car_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        parkingLot.park(car);
        Ticket wrongTicket = new Ticket(UUID.randomUUID().toString());

        //then
        Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLot.fetch(wrongTicket));

    }

    @Test
    public void should_return_nothing_when_fetch_given_a_car_and_fetched_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        //then
        Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLot.fetch(ticket));

    }

    @Test
    public void should_return_nothing_when_park_given_a_car_and_parked_three_cars_without_position() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();

        //when
        parkingLot.park(car1);
        parkingLot.park(car2);
        parkingLot.park(car3);

        //then
        Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car4));

    }
}
