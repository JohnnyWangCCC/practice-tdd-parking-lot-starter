package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class ParkingBoyTest {

    @Test
    public void should_return_ticket_when_park_given_a_car_and_two_parking_lots_both_available_and_a_boy(){
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        parkingLot1.setCapacity(1);
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.setCapacity(1);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1,parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = parkingLotBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);

    }

    @Test
    public void should_return_ticket_when_park_given_a_car_and_two_parking_lots_only_second_available_and_a_boy(){
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(1);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1,parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = parkingLotBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
        Assertions.assertTrue(parkingLot2.isIn(ticket));
    }

    @Test
    public void should_return_two_car_when_fetch_given_a_car_and_two_parking_lot_and_two_ticket_and_a_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(1);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLotBoy.park(car1);
        Ticket ticket2 = parkingLotBoy.park(car2);

        //when
        Car fetchedCar1 = parkingLotBoy.fetch(ticket1);
        Car fetchedCar2 = parkingLotBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);

    }

    @Test
    public void should_return_nothing_with_error_message_when_fetch_given_a_car_and_wrong_ticket_and_two_parking_lot_and_a_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(1);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        parkingLotBoy.park(car);
        Ticket wrongTicket = new Ticket(UUID.randomUUID().toString());

        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotBoy.fetch(wrongTicket));
        Assertions.assertEquals("UnRecognized parking ticket.", unRecognizedTicketException.getMessage());

    }

    @Test
    public void should_return_nothing_with_error_message_when_fetch_given_a_car_and_a_used_ticket_and_two_parking_lot() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(1);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = parkingLotBoy.park(car);
        parkingLotBoy.fetch(ticket);

        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotBoy.fetch(ticket));
        Assertions.assertEquals("UnRecognized parking ticket.", unRecognizedTicketException.getMessage());

    }

    @Test
    public void should_return_nothing_with_error_message_when_park_given_a_car_and_two_parking_lot_without_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(0);
        ParkingLotBoy parkingLotBoy = new ParkingLotBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when

        //then
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLotBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());

    }
}
