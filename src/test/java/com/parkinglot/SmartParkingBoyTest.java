package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class SmartParkingBoyTest {
    @Test
    public void should_return_ticket_when_park_given_a_car_and_two_parking_lots_both_available_and_a_smart_boy(){
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        parkingLot1.setCapacity(1);
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.setCapacity(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1,parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
        Assertions.assertTrue(parkingLot2.isIn(ticket));
    }

    @Test
    public void should_return_ticket_when_park_given_a_car_and_two_parking_lots_only_second_available_and_a_smart_boy(){
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1,parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
        Assertions.assertTrue(parkingLot2.isIn(ticket));
    }

    @Test
    public void should_return_two_car_when_fetch_given_a_car_and_two_parking_lot_and_two_ticket_and_a_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);

        Assertions.assertTrue(parkingLot2.isIn(ticket1));
        Assertions.assertTrue(parkingLot2.isIn(ticket2));

        //when
        Car fetchedCar1 = smartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = smartParkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);

    }

    @Test
    public void should_return_nothing_with_error_message_when_fetch_given_a_car_and_wrong_ticket_and_two_parking_lot_and_a_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        smartParkingBoy.park(car);
        Ticket wrongTicket = new Ticket(UUID.randomUUID().toString());

        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));
        Assertions.assertEquals("UnRecognized parking ticket.", unRecognizedTicketException.getMessage());

    }

    @Test
    public void should_return_nothing_with_error_message_when_fetch_given_a_car_and_a_used_ticket_and_two_parking_lot_and_a_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(1);
        parkingLot2.setCapacity(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);

        //then
        UnRecognizedTicketException unRecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("UnRecognized parking ticket.", unRecognizedTicketException.getMessage());

    }

    @Test
    public void should_return_nothing_with_error_message_when_park_given_a_car_and_two_parking_lot_without_position_and_a_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(0);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when

        //then
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());

    }

    @Test
    public void should_return_nothing_with_error_message_when_park_given_two_car_and_two_parking_lot_only_second_available_and_a_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        //then
        Assertions.assertDoesNotThrow(() -> smartParkingBoy.park(car1));
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(car2));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());

    }
}
